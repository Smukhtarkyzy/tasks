package dp.factory_method;

public class RoadLogistics implements Logistics{

    @Override
    public void deliver() {
        System.out.println("RoadLogistics was delivered!");
    }

}