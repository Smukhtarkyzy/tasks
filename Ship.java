package dp.factory_method;

public interface Ship {
    
 @Override
    public void deliver() {
        System.out.println("Ship was delivered!");
    }

}
