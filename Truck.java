package dp.factory_method;

public interface Truck {

 @Override
    public void deliver() {
        System.out.println("Truck was delivered!");
    }

}