package dp.factory_method;

import java.util.Scanner;

public class Main2 {
    public static Application configs(String logisticsType) {
        Application app;
        if(logisticsType.equals("Modern")) {
            app = new Application(new Sea Logistics());
        }else {
            app = new Application(new Road Logistics());
        }
        return app;
        }
        public static void main2(String args[]) {
        Scanner in = new Scanner(System.in);
        String logisticsType = in.next();
        Application app = configs(logisticsType);
        app.delivered();
    }
}