package dp.builder;

public interface HouseBuilder {
    Walls buildWalls();
    Doors buildDoors();
    Windows buildsWindows();
    Roof buildRoof();
    Garage buildGarage();
    
    public int getResult(){
        return House;
    }
}