package dp.factory_method;

public interface Transport {
    
 @Override
    public void deliver() {
        System.out.println("Transport was delivered!");
    }

}